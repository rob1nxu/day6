console.log("Starting...");
var express = require("express"); // "require" used -> server side , this code takes in express, a middleware as it interfaces the client side and the server
var bodyParser = require("body-parser"); //body parser will enable express to interpret the body of clientside index.html as a json
var app = express();

app.use(bodyParser.urlencoded({extended: false})); // code 6&7 registers bodyParser as a plugin
app.use(bodyParser.json());


console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.NODE_PORT || 3000;
var y = 0;

app.use(express.static(__dirname + "/../client/")); // tells express where is the ux ui directory and loads the relevant files
// once we are angular, the client file is no longer just static but has dynamic components. 

// app.use('/bower_components', express.static(__dirname + "/../client/bower_components")); 



function sum(x, z) {
    var y = x + z;
    return y;
}

app.get("/users", function(req, res) { 
// the "app" here is instantiated/activated at var app = express(); app refers to var app. 
//app.get when definining endpoints have to be ahead of app.use due to if, else if, else structure

    console.log("users");
    var person1 = {
        name: "Kenneth",
        age: 35
    };
    var person2 = {
        name: "Alvin",
        age: 40
    };
    var users = [person1, person2, person1, person1];
    res.json(users);
});

app.get("/students", function(req, res) {
    console.log("students");
    var person1 = {
        name: "Kenneth",
        age: 35
    };
    var person2 = {
        name: "Alvin",
        age: 40
    };
    var users = [person1, person2, person1, person1];
    res.send("My name is " + JSON.stringify(users[0]));
});

app.post("/users", function(req, res){

console.log("received user object " + req.body);
//console.log("received user object " + JSON.stringify(req.body)); //stringify is to covert json items into text
// console.log(req);

// var incomingReq = JSON.parse(req);
console.log("received user object " + JSON.stringify(req.body)); //stringify is to covert json items into text
var user =req.body;
user.email = "hi " + user.email;
console.log("email > " + user.email);
console.log("password > " + user.password);
console.log("fullname > " + user.fullname);
console.log("gender > " + user.gender);
console.log("date of birth > " + user.dateofbirth);
console.log("address > " + user.address);
console.log("nationality > " + user.nationality);
console.log("contact number > " + user.contactnumber);



res.status(200).json(user);

//res.status(200).json(req.body.user);

});





app.use(function(req, res, next) {
    y = 20 / 5;
    //res.send("<h1>Before the wrong door ! " + y + "</h1>");
    next();
});


app.use(function(req, res, next) {
    console.log(" sorry wrong door -> ");
    var x = sum(1, y);
    //res.send("<h1>Sorry wrong door ! " + x + "</h1>");
    next();
});

app.use(function(req, res) {
    console.log(" sorry wrong door -> ");
    var x = sum(1, y);
    res.send("<h1>The next thing ! " + x + "</h1>");
});

app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);
});