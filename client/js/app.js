(function () { //IIFE used -> client side 

    var app = angular.module("RegApp", []); //"RegApp" informs that this is a registration app. defines app as RegApp. a module

    app.controller("RegistrationCtrl", ["$http", RegistrationCtrl]); // i am naming this controlling as registration control and injected an angular component called $http

    // best practise, each controller is given a angular service eg.$http,etc

    function RegistrationCtrl($http) {
        var self = this; // vm, this for the controller to point to this variable. 

        self.user = {
        
            email: "",
            password: "",
            fullname: "",
            gender: "",
            dateofbirth: "",
            address: "",
            nationality: "",
            contactnumber: ""
        };


        self.initForm = function(){   //this is refresh the page
            self.user.email = "robinxu";
            console.log("page refresh")
        };


        self.initForm();
        

        self.displayUser = {
            email: "",
            password: "",
            fullname: "",
            gender: "",
            dateofbirth: "",
            address: "",
            nationality: "",
            contactnumber: ""
        };

        

        //self.age = 30; //this binds the controller to the index.html

        console.log(self.user.email);

        self.registerUser = function (){
            console.log(self.user.email);
            console.log(self.user.password);
            console.log(self.user.fullname);
            console.log(self.user.gender);
            console.log(self.user.dateofbirth);
            console.log(self.user.address);
            console.log(self.user.nationality);
            console.log(self.user.contactnumber);


            $http.post("/users", self.user) // promise here
                
                .then(function (result) {
                    console.log(result);
                    self.displayUser.email = result.data.email;
                    self.displayUser.password = result.data.password;
                    self.displayUser.fullname = result.data.fullname;
                    self.displayUser.gender = result.data.gender;
                    self.displayUser.dateofbirth = result.data.dateofbirth;
                    self.displayUser.address = result.data.address;
                    self.displayUser.nationality = result.data.nationality;
                    self.displayUser.contactnumber = result.data.contactnumber;
                })
                
                .catch(function (e) {
                    console.log(e);
                });
    
    };

};
    

})();

